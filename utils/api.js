/* eslint-disable prettier/prettier */
//http://181.59.45.37:8000/book/login/?user=Jorge&password=Silva
//const BASE_API = 'http://181.59.45.37:8000/book/login/'
const BASE_API = 'http://192.168.1.4:8084/wms/datos'
class Api {
    async logIn(user, pass){
        const settings = {
            method: 'GET',
        }
        const query = await fetch(`${BASE_API}/login/${user}/${pass}`, settings);
        const  data  = await query.json();
        return data; 
    }     
}
export default new Api();