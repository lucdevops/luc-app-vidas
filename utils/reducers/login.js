// Este es el lugar donde puedo cambiar mi estado. se ejecuta despues del dispatch
function videos(state={}, action){
    switch(action.type){
        case 'LOGIN': {
            return {...state, ...action.payload}
        }
        case 'SET_USER_INPUTTEXT': {
            return {...state, ...action.payload}
        }
        case 'SET_PASSWORD_INPUTTEXT': {
            return {...state, ...action.payload}
        }
        default:
            return state
    }
}
export default videos;