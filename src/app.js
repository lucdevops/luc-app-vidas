import React, { Component } from 'react';
import API from '../utils/api';
import { Router, Scene, Actions, ActionConst } from 'react-native-router-flux';
import { connect } from 'react-redux'
import Login from './screens/Login/components/LoginScreen';
import Main from './screens/Main/components/Main';


class AppLayout extends Component {

  async componentDidMount(){
    //Consulto y envio los datos al store
    try {
    //const login = await API.logIn();
    const login = false;
      this.props.dispatch({
        type:'LOGIN',
        payload:{
          login
        }
      })
    } catch (error) {
      console.error(error);
    } 
  }
    render(){
      return(
        <Router>
        <Scene key="root">
          <Scene key="loginScreen"
            component={Login}
            animation='fade'
            hideNavBar={true}
            initial={true}
          />
          <Scene key="mainScreen"
            component={Main}
            animation='fade'
            hideNavBar={true}
          />
        </Scene>
      </Router>
    )
  }
}

function mapStateToProps(state){
  return {
  }
}

export default connect(mapStateToProps)(AppLayout);