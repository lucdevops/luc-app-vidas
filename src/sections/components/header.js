import React from 'react';
import{
    View,
    Text,
    Image,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
} from 'react-native'

import {Actions, ActionConst} from 'react-native-router-flux';
function Header(props){

    const onPress = () => Actions.loginScreen();;
    return(
       <View >
           <SafeAreaView>
               <View style={style.container} >
                    <Image
                    source={require('../../screens/Login/images/logo3.png')}
                    style={style.logo}
                    />
               
               <View style={style.rigth} >
               <TouchableOpacity
                    style={style.button}
                    onPress={onPress}
                >
                <Text style={style.text}> Cerrar sesión </Text>
               </TouchableOpacity>
                    {props.children}
               </View>

               </View>
           </SafeAreaView>
       </View> 
    )
};

 const style = StyleSheet.create({
    logo:{
        width:60,
        height:50,
        resizeMode:'contain',
    },
    container:{
        padding:10,
        flexDirection:'row',
        backgroundColor:'#058FED',
    },rigth:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end'

    },
    text:{
        color:'white',
        padding:10

    },

 })
export default Header