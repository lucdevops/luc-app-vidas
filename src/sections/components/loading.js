import React from 'react';
import {
    View,
    Text, 
    Image,
StyleSheet,
ActivityIndicator
} from 'react-native';

function Loading (){
    return(
        <View styte={styles.container}>
            <Image
                source={require('../../../assets/logo.png')} 
                styte={styles.logo}
            />
            <ActivityIndicator
            
            />

        </View>
    )
}

const styles = StyleSheet.create(
    {
        container:{
            flex:1,//Todo el ancho disponible enla pantalla
            backgroundColor:'white', // Fondo Blanco
            alignItems: 'center',//Alineados elementos horizontalmente
            justifyContent: 'center' // alinación vertical
        },
        logo:{
            width:100,
            height:80,
            resizeMode:'contain',
            marginBottom:10,
        }

    }
)

export default Loading;