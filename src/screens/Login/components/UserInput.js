import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, TextInput, Image,Dimensions} from 'react-native';
import { connect } from 'react-redux'



function mapStateToProps(state){
  return{

  }
} 


class UserInput extends Component {

  fy = (text) =>
   {
     
    try {

    if (this.props.placeholder == 'Password'){
        const InputTextPassword = text;
        this.props.dispatch({
          type:'SET_PASSWORD_INPUTTEXT',
          payload:{
            InputTextPassword
            }
          })

     }else{
      const InputTextUser = text;
      this.props.dispatch({
        type:'SET_USER_INPUTTEXT',
        payload:{
          InputTextUser
        }
      })
     }
    } catch (error) {
      console.error(error);
    } 


   }

  render() {
    return (
      <View style={styles.inputWrapper}>
        <Image 
          source={this.props.source} 
          style={styles.inlineImg} />

        <TextInput
          style={styles.input}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          autoCorrect={this.props.autoCorrect}
          autoCapitalize={this.props.autoCapitalize}
          returnKeyType={this.props.returnKeyType}
          placeholderTextColor="white"
          underlineColorAndroid="transparent"
          onChangeText={this.fy}
        />
      </View>
    );
  }
}

UserInput.propTypes = {
  source: PropTypes.number.isRequired,
  placeholder: PropTypes.string.isRequired,
  secureTextEntry: PropTypes.bool,
  autoCorrect: PropTypes.bool,
  autoCapitalize: PropTypes.string,
  returnKeyType: PropTypes.string,
};

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    width: DEVICE_WIDTH - 40,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#ffffff',
  },
  inputWrapper: {
    padding:5
  },
  inlineImg: {
    position: 'absolute',
    zIndex: 99,
    width: 22,
    height: 22,
    left: 35,
    top: 15,
  },
});

export default connect(mapStateToProps)(UserInput)