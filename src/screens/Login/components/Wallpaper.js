import React, {Component} from 'react';
import {StyleSheet, ImageBackground} from 'react-native';



 class Wallpaper extends Component {
  render() {
    return (
      <ImageBackground 
        style={styles.picture} 
        source={require( '../images/wallpaper.png')}>
          {this.props.children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  picture: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
  },
});

export default Wallpaper;