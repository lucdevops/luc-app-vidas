import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';


export default class Logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image 
            source={require('../images/logo3.png')} 
            style={styles.image} />
            <Text 
            style={styles.text} >
               Gestión Oportuna 
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 100,
    height: 100,
    resizeMode:'contain',

  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    fontSize: 25,
    marginTop: 10,
    marginBottom: 15,
  },
});
