import React, {Component} from 'react';
import Wallpaper from './Wallpaper';
import Logo from './Logo';
import Form from './Form';
import SignupSection from './SignupSection';
import ButtonSubmit from './ButtonSubmit';
import { connect } from 'react-redux'

function mapStateToProps(state){
//  console.log(state.login)
  return{}
} 

class LoginScreen extends Component {
  render() {
    return (
      <Wallpaper>
        <Logo/>
        <Form/>
        <ButtonSubmit/>
      </Wallpaper>
    );
  }
}


export default connect(mapStateToProps)(LoginScreen);