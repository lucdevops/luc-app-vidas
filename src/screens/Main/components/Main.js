import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Text,

} from 'react-native';

import Header from '../../../sections/components/header';
import API from '../../../../utils/api';

import {Actions, ActionConst} from 'react-native-router-flux';
//Extraer datos desde el estado
function mapStateToProps(state) {
  return {
    InputTextPassword: state.InputTextPassword,
    InputTextUser: state.InputTextUser,
  };
}

const SIZE = 40;
 class Main extends Component {
  render() {
    return (
      <View style={styles.container} >
        <Header>
        </Header>
        <Text> Bienvenido {this.props.InputTextUser} </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{

  }
});



export default connect(mapStateToProps)(Main);