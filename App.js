import React, {Component} from 'react';

import AppLayout from './src/app';


import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import  { store, persistor}  from  './utils/store';
import  Loading  from  './src/sections/components/loading';


class App extends Component {
  render(){
    return(
      <Provider 
      store={store} >
        <PersistGate
          persistor={persistor}
          >
          <AppLayout/>
        </PersistGate>
      </Provider>
    )
  }
} 
export default App;
